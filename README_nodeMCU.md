# NODEMCU V2 #

This README document the steps to get your application up and running.




### What is this board? ###

* Programmable microcontroller
* Generate input from buttons, output with leds,...


* Wi-Fi Module – ESP-12E
* micro USB port; power and programming
* Headers, compatible with breadboards
* Operates at 3.3V 

* We use it as an Arduino with integrated wifi

### How do set it up on my PC? ###

* We will use the Arduino environment  
* Configuration: See Video

* source video: https://www.instructables.com/Quick-Start-to-Nodemcu-ESP8266-on-Arduino-IDE/ 


links: 

* [Arduino.cc](https://www.arduino.cc/) (Integrated Development Environment)

* [JsonFile](https://bitbucket.org/SeppeSels/wetenschappelijkproject/raw/ae2e43a357c830e329a8decbbcfc518b9f1c3b6c/Boards/NodeMCU/package_esp8266com_index.json)

* [JsonFile_Arduino] (http://arduino.esp8266.com/stable/package_esp8266com_index.json)

### Pinout ###

The labels on the board do not correspond to the labels used in the arduino IDE.

In the figure below you can find the conversion

![NODEMCUV2](Boards/NodeMCU/NodeMCU_V2_v2.png)

[Source](https://mechatronicsblog.com/esp8266-nodemcu-pinout-for-arduino-ide/) 



### Who do I talk to? ###

*  team contact